import { exists, join, resolve } from '../deps.ts'
import { Option, some, none } from '../deps.ts'

const findFileReversed = async (
  fileName: string,
  startingPath: string = '.'
): Promise<Option<string>> => {
  const filePath = resolve(join(startingPath, fileName))
  const fileExist = await exists(filePath)

  if (fileExist) {
    return some(filePath)
  }

  const parentDir = resolve(join(startingPath, '..'))

  if (parentDir === resolve(startingPath)) {
    return none()
  }

  return await findFileReversed(fileName, parentDir)
}

export { findFileReversed }
