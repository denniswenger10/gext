type Result<DataType> = [boolean, DataType]

const createResult = <DataType>(
  data: DataType,
  isSuccess: boolean = false
): Result<DataType> => {
  return [isSuccess, data]
}

export { createResult }
export type { Result }
