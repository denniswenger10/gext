import { Config } from '../lib/model/mod.ts'
import { runCmd } from '../lib/run-cmd/mod.ts'

const setUser = async ({ name, email }: Config) => {
  if (name.length > 0) {
    await runCmd(['git', 'config', 'user.name', name])
  }

  if (email.length > 0) {
    await runCmd(['git', 'config', 'user.email', email])
  }
}

export { setUser }
