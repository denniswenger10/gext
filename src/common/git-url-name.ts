import { lastOr } from '../common/mod.ts'
import { some, none, Option } from '../deps.ts'

const gitUrlName = (gitUrl: string): Option<string> => {
  const projectName = lastOr('')(gitUrl.split('/')).replace(
    '.git',
    ''
  )

  return projectName.length > 0 ? some(projectName) : none()
}

export { gitUrlName }
