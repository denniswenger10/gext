import { assertEquals } from '../../../deps.ts'
import { replaceHost } from './replace-host.ts'
import { createConfig } from '../../model/mod.ts'

Deno.test('Replace host in repository address', () => {
  const config = createConfig({ sshHost: 'gl-test' })

  const testList: [string, string][] = [
    [
      'git@gitlab.com:denniswenger10/headless-pi-setup.git',
      'gl-test:denniswenger10/headless-pi-setup.git',
    ],
  ]

  testList.forEach(([from, to]) => {
    assertEquals(replaceHost(config, from), to)
  })
})
