import { Config } from '../../model/mod.ts'

const replaceHost = (config: Config, repository: string): string => {
  const [_, ...rest] = repository.split(':')

  return [config.sshHost, ...rest].join(':')
}

export { replaceHost }
