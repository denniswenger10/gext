import { Config } from '../model/mod.ts'
import { runCmd } from '../run-cmd/mod.ts'
import { gitUrlName, setUser } from '../../common/mod.ts'
import { replaceHost } from './replace-host/mod.ts'

/**
 * The same as a normal "git clone" but will replace the url with the host from config file, and set the email and name from the config file.
 *
 * Example:
 * ```json5
 * // .gext-config
 * {
 *    "host": "private",
 *    "name": "John Doe",
 *    "email": "john@doe.com"
 * }
 * ```
 * 'git@gitlab.com:john-doe/cool-project.git'
 *  => 'private:john-doe/cool-project.git'
 *
 * "John Doe" is set as user.name.
 *
 * "john@description.com" is set as user.email
 */
const clone = (config: Config) => async (repository: string) => {
  try {
    const replacedHost =
      config.sshHost.length > 0
        ? replaceHost(config, repository)
        : repository

    await runCmd(['git', 'clone', replacedHost])

    gitUrlName(repository).map(async (projectFolder) => {
      await runCmd(['cd', projectFolder])
      await setUser(config)
    })
  } catch (err) {
    console.error('Was unable to clone the repository.', err)
  }
}

export { clone }
