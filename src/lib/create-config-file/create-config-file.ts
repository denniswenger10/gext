import { Config } from '../model/mod.ts'
import { CONSTANTS } from '../constants.ts'
import { join, resolve, exists } from '../../deps.ts'

const createConfigFile = async (config: Config, path = '.') => {
  try {
    const configFilePath = join(
      resolve(path),
      CONSTANTS.configFileName
    )

    if (await exists(configFilePath)) {
      console.log(
        "It seems there's already a config file at that location. We suggest you edit that one."
      )
      return
    }

    await Deno.writeTextFile(
      configFilePath,
      JSON.stringify(config, null, 2)
    )
  } catch (err) {
    console.error('Was unable to write config file.', err)
  }
}

export { createConfigFile }
