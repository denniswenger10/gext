import { exists, some, none, Option } from '../../deps.ts'
import { filterMap } from '../../common/mod.ts'
import { sshConfiglocations } from './ssh-config-locations.ts'
import { findSshConfigFile } from './find-ssh-config-file.ts'

const getHostNames = (text: string): string[] =>
  filterMap((row) => {
    if (row.startsWith('Host ')) {
      return row.replace('Host ', '')
    }
  }, text.split(/\r?\n/))

const getSshHosts = async () =>
  (await findSshConfigFile()).match({
    some: async (sshConfigPath) =>
      getHostNames(await Deno.readTextFile(sshConfigPath)),
    none: async () => [],
  })

export { getSshHosts }
