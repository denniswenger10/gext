import { exists, some, none, Option } from '../../deps.ts'
import { sshConfiglocations } from './ssh-config-locations.ts'

const findSshConfigFile = async (): Promise<Option<string>> => {
  for (const location of sshConfiglocations) {
    const fileExist = await exists(location)
    if (fileExist) {
      return some(location)
    }
  }

  return none()
}

export { findSshConfigFile }
