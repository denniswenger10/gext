const sshConfiglocations = [
  `${Deno.env.get('HOME')}/.ssh/config`,
  '/etc/ssh/ssh_config',
]

export { sshConfiglocations }
