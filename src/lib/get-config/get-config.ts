import { findFileReversed } from '../../common/mod.ts'
import { Option, some, none } from '../../deps.ts'
import { createConfig, Config } from '../model/mod.ts'
import { CONSTANTS } from '../constants.ts'

// TODO: Handle case where there are no configs

const getConfig = async (): Promise<Option<Config>> => {
  try {
    const confPathOption = await findFileReversed(
      CONSTANTS.configFileName
    )

    const conf = await confPathOption.match({
      some: async (confPath) => {
        const confText = await Deno.readTextFile(confPath)

        return createConfig(JSON.parse(confText))
      },
      none: async () => createConfig({}),
    })

    const hasAllRequired = [
      conf.name.length > 0,
      conf.email.length > 0,
    ].every((p) => p)

    return confPathOption.isSome() && hasAllRequired
      ? some(conf)
      : none()
  } catch {
    return none()
  }
}

export { getConfig }
