interface MaybeConfig {
  name?: string
  email?: string
  sshHost?: string
}

interface Config {
  name: string
  email: string
  sshHost: string
}

const createConfig = (config: MaybeConfig) => {
  const { name = '', email = '', sshHost = '' } = config

  return {
    name,
    email,
    sshHost,
  }
}

export type { MaybeConfig, Config }
export { createConfig }
