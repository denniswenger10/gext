import { Command } from '../../deps.ts'
import { setUserAction } from './set-user-action.ts'

const setUserCommand = new Command()
  .description('')
  .action(setUserAction)

export { setUserCommand }
