import { setUser } from '../../common/mod.ts'
import { getConfig } from '../../lib/get-config/mod.ts'
import { CONSTANTS } from '../../lib/constants.ts'

const setUserAction = async (options: any) => {
  const configOption = await getConfig()

  configOption.mapOrElse(
    async (config) => {
      await setUser(config)
    },
    () => {
      console.error(
        `Was unable to find ${CONSTANTS.configFileName} in this or any parent directory. 
        You can create a config by running gext create-config`
      )
    }
  )
}

export { setUserAction }
