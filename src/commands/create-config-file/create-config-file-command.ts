import { Command } from '../../deps.ts'
import { createConfigFileAction } from './create-config-file-action.ts'

const createConfigFileCommand = new Command()
  .description('')
  .arguments<[path?: string]>('[path]')
  .option<{ name?: string }>('-n, --name', 'a desc...')
  .option<{ email?: string }>('-m, --email', 'a desc...')
  .option<{ sshHost?: string }>('-s, --ssh-host', 'a desc...')
  .action(createConfigFileAction)

export { createConfigFileCommand }
