import { createConfigFile } from '../../lib/create-config-file/mod.ts'
import { Input, Select } from '../../deps.ts'
import { createConfig, MaybeConfig } from '../../lib/model/mod.ts'
import { getSshHosts } from '../../lib/ssh-hosts/mod.ts'

type CreateConfigOptions = MaybeConfig

const createConfigFileAction = async (
  options: CreateConfigOptions = {},
  path = '.'
) => {
  try {
    const config = createConfig({
      name: options.name ?? (await Input.prompt("What's your name?")),
      email:
        options.email ?? (await Input.prompt("What's your email?")),
      sshHost:
        options.sshHost ??
        (await Select.prompt({
          message:
            'What is the host name you use in your "~/.ssh/config?"',
          options: await getSshHosts(),
        })),
    })

    createConfigFile(config, path)
  } catch (err) {
    console.error('Was unable to write config file.', err)
  }
}

export { createConfigFileAction }
