import { clone } from '../../lib/clone/mod.ts'
import { getConfig } from '../../lib/get-config/mod.ts'
import { CONSTANTS } from '../../lib/constants.ts'

const cloneAction = async (options: any, repository: string) => {
  const configOption = await getConfig()

  configOption.mapOrElse(
    async (config) => {
      await clone(config)(repository)
    },
    () => {
      console.error(
        `Was unable to find ${CONSTANTS.configFileName} in this or any parent directory. 
        You can create a config by running gext create-config`
      )
    }
  )
}

export { cloneAction }
