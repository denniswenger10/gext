import { Command } from '../../deps.ts'
import { cloneAction } from './clone-action.ts'

const cloneCommand = new Command()
  .arguments('<repository:string>')
  .description('')
  .action(cloneAction)

export { cloneCommand }
