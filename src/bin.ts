import { Command } from './deps.ts'
import {
  cloneCommand,
  createConfigFileCommand,
  setUserCommand,
} from './commands/mod.ts'

await new Command()
  .description('Yada yada')
  .command('clone', cloneCommand)
  .command('create-config', createConfigFileCommand)
  .command('set-user', setUserCommand)
  .parse(Deno.args)
